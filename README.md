# Programming Exercises

## Exercise booklet

### Variables, input, output
1. Printing practice
Write a plan that prints the data of the following rectangle: Length: 2 cm and width 3 cm.
The print template will look like this:
The rectangle properties are:
Length: 2 centimeters.
Width: 3 centimeters.

2. Printing practice
Write a program that prints the following student details:
Name: Ran Chen
Address: Habanim 35 Herzelia
Telephone: 050-8754522
Age: 24
Faculty: Computer Science
Institution: Mla
Average: 78.5

3. Printing practice
Write a program that prints the following structure using a single print command:
A B
A B C
A B C D

4. Printing practice
Write a program that prints the following structure using a single print command:
A, B
A, B, C
A, B, C, D

5. Practice variable information acquisition and printing
Write a program that captures from the user the length and width of a rectangle and prints the data.
For example, if the length is 2 cm and the width is 3 cm, the data should be printed in the following format:
The rectangle properties are:
Length: 2 centimeters.
Width: 3 centimeters.

6. Practice absorbing information for variables and printing
Write a program that asks for the following details from the user: first name and last name, address, telephone, age, faculty, educational institution and grade point average.
The program will print the data in the format below as an example:
Name: Ran Chen
Address: Habanim 35 Herzelia
Telephone: 050-8754522
Age: 24
Faculty: Computer Science
Institution: Mla
Average: 78.5

7. Practice absorbing information for variables, applying a formula and printing
Write a program that asks the user for the length and width of a rectangle and calculates the following data:
Circle the rectangle
Rectangle area.
The data must be printed in the following format:
Rectangle properties are: 2 centimeters length and 3 centimeters width
circumference: 10 centimeters
Area: 6 centimeters

8. Practice absorbing information for variables, applying a formula and printing
Write a program that receives from the user name, score (in whole values) and factor (in percent) and prints the final score after the factor (1 digit must be displayed after the dot)
Example:
The score before the factor was 67 and the factor 8%. To calculate the final score we will use the following formula:
67 * (1 + 8/100) = 72.36
Name: Miri Dekel
Grade: 72.3
Factor: 8

9. Practice absorbing information for variables, running a formula and printing
One company found that its annual profit is 23 percent of total sales. Write a plan that asks the user to enter the expected amount of total sales, then display the annual profit.
Example:
Sales: 100,000
To calculate the annual profit considered: 100,000 * (23/100)
The printout will look like this:
For estimated sales: 100,000 annual profits estimate is 23,000

10. Practice absorbing information for variables, applying a formula and printing
A customer in the store purchases five items. Write content that asks for the price of each item and then prints the total account before tax, the tax itself, and the total account after tax. Assume the tax is 7 percent.

11. Practice absorbing information for variables, applying a formula and printing
Write a plan that calculates travel distances. The formula for calculating travel distance is:
Distance = speed per km / h * Time in hours.
A car travels 80 km / h. Write a plan that shows the following distances:
The distance the car will travel for 6 hours
The remote that the car will travel for 10 hours
The distance the car will travel for 15 hours
Printing should look like this
Time (hours) Distance (km)
-----------------------------------------------
  6 480
10 800
15 1200


12. Practice absorbing information for variables, running a formula and printing
Write a program that translates temperature in Celsius to temperature in Fahrenheit.
The formula is:

The program will ask the user to type a temperature in Celsius and then display a Fahrenheit temperature.

13. Practice absorbing information for variables, running a formula and printing
A cake recipe includes the following ingredients:
- 1.5 cups of sugar
- 1 Butter
- 2.75 flour.

Write a plan that asks the user how many cakes he wants to make, and show the amount of ingredients for all the cakes.

14. Practice absorbing information for variables, applying a formula and printing
Write a program that asks the user to have a number of men and a number of women in the class. The program will print the percentage of men and the percentage of women in the class.
For example:
Suppose there are 8 women and 12 men. In total there are 20 students.
The percentage of women is (8:20) * 100 i.e. 40%.
The percentage of men is (12:20) * 100 ie 60%


### Higher difficulty questions


15. Write a plan that receives from the user distance in kilometers and speed in km / h and calculates the time of arrival in minutes and hours to the destination.

16. Write a program that receives a double-digit number from the user and prints the sum of his digits.

17. In writing a program that receives a three-digit number from the user and prints the sum of his digits.

18. Write a plan that takes two vertices of a right-angled triangle whose one vertex is at (0,0) and prints the area of ​​the triangle. For example, for the values- (0,4), (4,0) the program will print the number 8

### Legal terms
1. Write a program that asks the user for a number in the range of 1 to 7. If the number obtained is less than 1 or greater than 7, an error message must be given. The plan should print the appropriate day in the following order: 1 = Sunday, 2 = Monday, ….7 = Saturday

2. Write a program that asks the user for the length and width of two rectangles and prints for those who have a larger area (calculation area of ​​rectangles = width * length).

3. Write a plan that checks whether a customer of the bank is entitled to a loan. A client is entitled to a loan if his salary is greater than 30,000 and his seniority in work is greater than or equal to two years. The plan will receive from the user the details of the salary and the number of years of seniority and will print out whether he is entitled to a loan or not.

4. Write a program that converts a score to a category according to American scores, according to the following key:
- If the score is greater than or equal to 90 the classification is A.
- If the score is greater than or equal to 80 and less than 90 the classification is B.
- If the score is greater than or equal to 70 and less than 80 the classification is C.
- If the score is less than 70 the class is D.
The program will receive a score from the user and convert it to an American classification.
Exercise booklet

Variables, input, output
1. Printing practice
Write a plan that prints the data of the following rectangle: Length: 2 cm and width 3 cm.
The print template will look like this:
The rectangle properties are:
Length: 2 centimeters.
Width: 3 centimeters.

2. Printing practice
Write a program that prints the following student details:
Name: Ran Chen
Address: Habanim 35 Herzelia
Telephone: 050-8754522
Age: 24
Faculty: Computer Science
Institution: Mla
Average: 78.5

3. Printing practice
Write a program that prints the following structure using a single print command:
A B
A B C
A B C D

4. Printing practice
Write a program that prints the following structure using a single print command:
A, B
A, B, C
A, B, C, D

5. Practice variable information acquisition and printing
Write a program that captures from the user the length and width of a rectangle and prints the data.
For example, if the length is 2 cm and the width is 3 cm, the data should be printed in the following format:
The rectangle properties are:
Length: 2 centimeters.
Width: 3 centimeters.

6. Practice absorbing information for variables and printing
Write a program that asks for the following details from the user: first name and last name, address, telephone, age, faculty, educational institution and grade point average.
The program will print the data in the format below as an example:
Name: Ran Chen
Address: Habanim 35 Herzelia
Telephone: 050-8754522
Age: 24
Faculty: Computer Science
Institution: Mla
Average: 78.5

7. Practice absorbing information for variables, applying a formula and printing
Write a program that asks the user for the length and width of a rectangle and calculates the following data:
Circle the rectangle
Rectangle area.
The data must be printed in the following format:
Rectangle properties are: 2 centimeters length and 3 centimeters width
circumference: 10 centimeters
Area: 6 centimeters

8. Practice absorbing information for variables, applying a formula and printing
Write a program that receives from the user name, score (in whole values) and factor (in percent) and prints the final score after the factor (1 digit must be displayed after the dot)
Example:
The score before the factor was 67 and the factor 8%. To calculate the final score we will use the following formula:
67 * (1 + 8/100) = 72.36
Name: Miri Dekel
Grade: 72.3
Factor: 8


9. Practice absorbing information for variables, running a formula and printing
One company found that its annual profit is 23 percent of total sales. Write a plan that asks the user to enter the expected amount of total sales, then display the annual profit.
Example:
Sales: 100,000
To calculate the annual profit considered: 100,000 * (23/100)
The printout will look like this:
For estimated sales: 100,000 annual profits estimate is 23,000

10. Practice absorbing information for variables, applying a formula and printing
A customer in the store purchases five items. Write content that asks for the price of each item and then prints the total account before tax, the tax itself, and the total account after tax. Assume the tax is 7 percent.

11. Practice absorbing information for variables, applying a formula and printing
Write a plan that calculates travel distances. The formula for calculating travel distance is:
Distance = speed per km / h * Time in hours.
A car travels 80 km / h. Write a plan that shows the following distances:
- The distance the car will travel for 6 hours
- The remote that the car will travel for 10 hours
- The distance the car will travel for 15 hours
Printing should look like this
Time (hours) Distance (km)
-----------------------------------------------
  6 480
10 800
15 1200


12. Practice absorbing information for variables, running a formula and printing
Write a program that translates temperature in Celsius to temperature in Fahrenheit.
The formula is:

The program will ask the user to type a temperature in Celsius and then display a Fahrenheit temperature.

13. Practice absorbing information for variables, running a formula and printing
A cake recipe includes the following ingredients:
- 1.5 cups of sugar
- 1 Butter
- 2.75 flour.

Write a plan that asks the user how many cakes he wants to make, and show the amount of ingredients for all the cakes.

14. Practice absorbing information for variables, applying a formula and printing
Write a program that asks the user to have a number of men and a number of women in the class. The program will print the percentage of men and the percentage of women in the class.
For example:
Suppose there are 8 women and 12 men. In total there are 20 students.
The percentage of women is (8:20) * 100 i.e. 40%.
The percentage of men is (12:20) * 100 ie 60%




### Higher difficulty questions


15. Write a plan that receives from the user distance in kilometers and speed in km / h and calculates the time of arrival in minutes and hours to the destination.

16. Write a program that receives a double-digit number from the user and prints the sum of his digits.


17. In writing a program that receives a three-digit number from the user and prints the sum of his digits.


18. Write a plan that takes two vertices of a right-angled triangle whose one vertex is at (0,0) and prints the area of ​​the triangle. For example, for the values- (0,4), (4,0) the program will print the number 8



Legal terms
1. Write a program that asks the user for a number in the range of 1 to 7. If the number obtained is less than 1 or greater than 7, an error message must be given. The plan should print the appropriate day in the following order: 1 = Sunday, 2 = Monday, ….7 = Saturday

2. Write a program that asks the user for the length and width of two rectangles and prints for those who have a larger area (calculation area of ​​rectangles = width * length).

3. Write a plan that checks whether a customer of the bank is entitled to a loan. A client is entitled to a loan if his salary is greater than 30,000 and his seniority in work is greater than or equal to two years. The plan will receive from the user the details of the salary and the number of years of seniority and will print out whether he is entitled to a loan or not.


4. Write a program that converts a score to a category according to American scores, according to the following key:
- If the score is greater than or equal to 90 the classification is A.
- If the score is greater than or equal to 80 and less than 90 the classification is B.
- If the score is greater than or equal to 70 and less than 80 the classification is C.
- If the score is less than 70 the class is D.
The program will receive a score from the user and convert it to an American classification.

5. A salesman receives a base salary of NIS 6,000 per month and another bonus. The bonus calculation is as follows:
a. Sales up to NIS 500:
A bonus of 10% of total sales
b. Sales in the amount of 501-1500: a bonus of 20% of total sales
c. Sales of 1501 or more: A bonus of 30% of total sales
Write a plan that accepts the salesperson's total sales, and prints the monthly salary.
6. Write a program that asks the user to type in the person's age. The program should print out whether the person is an infant, child, adolescent, or adult. (infant, child, teenager, adult). The association is made according to the following general:
a. If the person is a year or less - he is considered a baby
b. If the person is older than 1 year and younger than 13 - he is considered a child
c. If the person is 13 years old or older but younger than 20 - he is considered a teenager
d. If the person is 20 years old or older - he is considered an adult.

7. The date of June 10, 1960 is a special date because if you write it in the following way 6/10/60 you see that today * month = year. Write a plan in which you ask the user for the day, month and year in two digits. If the date is special according to the formula we wrote down the program will print: the day is magic. Otherwise the program will print: the day is not magic.

8. An airline is looking for security personnel. Admission requirements are: age between 20 and 40 or (age 30 to 50 and height over 180 cm). Write a plan that accepts the age and height of a candidate and prints if he has been accepted.

9. A software store sells a software package for $ 99. The store offers a discount depending on the quantity of packages purchased according to the following table:

Discount quantity
10-19 10%
20-49 20%
50-99 30%
100 and more 40%

Write a program that asks the user for a quantity of packages and shows the price before discount, the discount and price after discount.

### Higher difficulty questions


10. Write a program that receives two numbers from the user and prints the minimum number

11. Write a program that receives three numbers from the user and prints the maximum number.

12. Write a program that receives two numbers from the user and checks whether they are divided by each other

13. Write a program that receives from the user a number between two digits and checks whether it contains the digit 5.

14. Write a time calculator. The user has to enter a number of seconds. The calculator works as follows:

a. There are 60 seconds per minute. If the number of seconds is greater than or equal to 60, the calculator must display the number of minutes that enter.
b. There are 3600 seconds per hour. If the number of seconds is greater than or equal to 3600, the calculator must display the number of hours entered.
c. There are 86,400 seconds a day. If the number of seconds is greater than or equal to 86,400, the calculator must display the number of days.

### Loops
1. Write a plan that prints the numbers from 1 to 10 in ascending order. The numbers must be printed with commas and one space between the numbers: 1, 2, 3….

2. Write a plan that prints the numbers 1 to 10 in descending order (i.e. from 10 to 1). The numbers should be printed with commas and one spirit between the numbers. 10, 9, 8 ..

3. Write a program that picks up 10 integers and prints the highest number.

4. Write a program that picks up 10 numbers and prints the lowest number.

5. Write a plan that picks up 10 numbers and prints their sum.

6. Write a plan that picks up 10 numbers and prints their average.

7. Write a program that receives numbers from the user until a negative number is obtained. The program will print the sum of the numbers received.

8. Write a program that prints the sum of the numbers up to a number entered by the user. For example, if the user enters the number 10, a program must be written that prints the sum of 1 + 2 + 3 +… + 10

9. Write a plan that ran 5 times. Each time it asks for the number of bags collected and finally prints the total bags collected.

10. A particular sports program burns 4.2 calories per minute. Write a program that runs in a loop and prints the number of calories burned after 10,15,20,25,30 minutes.

11. A program address that asks the user for his budget in shekels per month. Write a loop that asks the user to enter all of his expenses in a particular month. The loop ends as soon as the user says he has no more expenses. Sum up all the expenses and check if they exceeded the budget or not and print an appropriate message.

12. Write a program that translates temperature in Celsius to temperature in Fahrenheit. The formula is:
  The program will display a table of degrees from 0 20 in Celsius and their translation to Fahrenheit:

Celsius Fahrenheit
0 32
1 33.8
2 35.6

13. Write a program that asks the user for the speed of a vehicle and the number of hours he drove, and prints the distance traveled each hour. The formula for calculating distance is speed * time. Printable example:
Speed: 50
Hours: 3
Hours Distance
1 50
2 100
3 150
14. Write a plan that calculates how much a person will earn in 30 hours when for the first hour he earns 1 shekel, for the second hour he earns 2 shekels, for the third hour he earns 3 shekels and so on.


Higher difficulty questions


15. Write a plan that prints the following pattern:
```sh
*
* *
* * *
* * * *
* * * * *
* * * * * *
```
16. Write a plan that prints the following pattern:
```sh
#
  #
     #
        #
           #
```
17. Write a plan that prints the following pattern:
```sh
* * * * * *
* * * * *
* * * *
* * *
* *
*
```
18. Write a plan that prints the following pattern:
```sh
##
# #
#  #
#   #
#    #
#     #
```

19. Write a program that asks the user for 10 numbers and summarizes each year of them during the reception, and prints the amount each time.

20. Write a program that asks the user for a number and prints the number of his digits. Follow this plan by dividing the number by 10 each time until it is equal to zero.

21. Write a program that asks the user for a number and prints the sum of his digits.

22. Write a plan that shows all the numbers between 1 and 100 divided by 7.

23. Write a program that receives a number from the user and checks whether it contains the number 5.


### Strings
1. Write a program that receives a string from the user and checks if the letter d appears in the string.

2. Write a program that receives a string from the user, turns the letters into an upper case and saves in a new string.

3. Write a program that receives a string from the user and prints whether the string is a number.

4. Write a program that picks up a string from the user and prints the first three letters and the last three letters in the string.

5. Write a program that picks up a string from the user and prints all the letters below


